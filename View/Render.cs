﻿using OOPLab6.Model.classes.roads.road_bands;
using OOPLab6.Model;
using OOPLab6.Model.classes.helpers.FinalResults;
using OOPLab6.Model.classes.road_objects.crosswalks;

namespace OOPLab6.View;

/// <summary>
/// View component of the simulation
/// </summary>
public class Render : IRender {
	private readonly IConfig _config;

	public Render(IConfig config) {
		_config = config;
	}

	/// <summary>
	/// Shows the final message after the simulation
	/// </summary>
	/// <param name="finalResult">The results string to bew shown</param>
	public void DrawEndScreen(IFinalResult finalResult) {
		Console.Clear();
		Console.ForegroundColor = ConsoleColor.DarkCyan;
		Console.Write(finalResult);
		Console.ResetColor();
		Console.WriteLine("\nPress any key to exit\n");
		Console.ReadKey(true);
	}

	/// <summary>
	/// Renders the given band according to config
	/// </summary>
	/// <param name="band"></param>
	public void DrawBand(IRoadBand band) {
		Console.Clear();

		for (int i = _config.View.From; i < _config.View.To; i++) {
			if (band.HasCrossWalkAt(i)) {
				ICrosswalk walk = band.Crosswalks[i]!;

				for (int j = 0; j < _config.BandWidth + 2; j++) {
					if (band.HasCarAt(i) && j == _config.BandWidth / 2 + 1) {
						_drawCar();
						continue;
					}

					Console.BackgroundColor = j % 2 == 0
						? ConsoleColor.White
						: ConsoleColor.Black;

					Console.Write(" ");
					Console.ResetColor();
				}

				Console.Write($" {walk.PedsCount}");
			}
			else {
				Console.ResetColor();
				Console.Write("|");
				for (int j = 0; j < _config.BandWidth; j++) {
					if (band.HasCarAt(i) && j == _config.BandWidth / 2) {
						_drawCar();
						Console.ResetColor();
						continue;
					}

					Console.Write(" ");
				}

				Console.Write("|");
			}

			if (i == _config.View.From) {
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.Write("   Press ");

				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write('Q');

				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.Write(" to quit simulation.");
			}

			if (band.HasTrafficLightAt(i))
				_drawTrafficLight(band.TrafficLights[i]!.IsGreen);

			Console.ResetColor();
			Console.WriteLine();
		}
	}

	private static void _drawTrafficLight(bool isGreen) {
		Console.Write(" ");
		Console.BackgroundColor = isGreen ? ConsoleColor.Green : ConsoleColor.Red;
		Console.Write(" ");
		Console.ResetColor();
		Console.Write(" ");
	}

	private static void _drawCar() {
		Console.BackgroundColor = ConsoleColor.DarkBlue;
		Console.ForegroundColor = ConsoleColor.Black;
		Console.Write("*");
		Console.ResetColor();
	}
}
