﻿using OOPLab6.Model.classes.helpers.FinalResults;
using OOPLab6.Model.classes.roads.road_bands;

namespace OOPLab6.View;

public interface IRender {
	void DrawEndScreen(IFinalResult finalResultDto);
	void DrawBand(IRoadBand   band);
}
