﻿using OOPLab6.Presenter;
using OOPLab6.Model;
using OOPLab6.View;

Config   config   = new();
Render   render   = new(config);
Simulate simulate = new(config, render);

simulate.Run();
