# OOP Lab 6
## Dobrojan Alexandru FAF-212

---

In this laboratory work I refactor all my code to match modern programming principles and also divide the simuation in 3 components 
Model, View and Presenter (MVP).\
Here the Model is all the data from the configuration file, the View is the displaying 
class and the Provider is the Simulation class.
