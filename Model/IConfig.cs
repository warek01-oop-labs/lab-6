﻿using OOPLab6.Model.config_dtos;

namespace OOPLab6.Model;

public interface IConfig {
	public bool IsInfinite       { get; }
	public int  BandWidth        { get; }
	public int  TicksCount       { get; }
	public int  TickSleepMs      { get; }
	public int  PedCrossingTicks { get; }
	public int  BandLength       { get; }

	public IRange View            { get; }
	public IRange CarFlowCooldown { get; }

	public List<ITrafficLightConfig>          TrafficLights        { get; }
	public List<IUncontrolledCrosswalkConfig> CrossWalks           { get; }
	public List<IControlledCrosswalkConfig>   ControlledCrossWalks { get; }
}
