﻿using System.Text;
using System.Text.Json.Nodes;
using OOPLab6.Model.config_dtos;

namespace OOPLab6.Model;

/// <summary>
/// Model component of the simulation
/// </summary>
public class Config : IConfig {
	#region Declare DTO classes

	public class Range : IRange {
		public int From { get; }
		public int To   { get; }

		public Range(int from, int to) {
			From = from;
			To   = to;
		}
	}

	public class TrafficLightConfig : ITrafficLightConfig {
		public int At            { get; }
		public int GreenDuration { get; }
		public int RedDuration   { get; }

		public TrafficLightConfig(int at, int greenDuration, int redDuration) {
			At            = at;
			GreenDuration = greenDuration;
			RedDuration   = redDuration;
		}
	}

	public class UncontrolledCrossWalkConfig : IUncontrolledCrosswalkConfig {
		public int At             { get; }
		public int MinPedCooldown { get; }
		public int MaxPedCooldown { get; }

		public UncontrolledCrossWalkConfig(int at, int min, int max) {
			At             = at;
			MinPedCooldown = min;
			MaxPedCooldown = max;
		}
	}

	public class ControlledCrossWalkConfig : IControlledCrosswalkConfig {
		public int At             { get; }
		public int MinPedCooldown { get; }
		public int MaxPedCooldown { get; }
		public int GreenTiming    { get; }
		public int RedTiming      { get; }

		public ControlledCrossWalkConfig(int at, int min, int max, int greenTiming, int redTiming) {
			At             = at;
			MinPedCooldown = min;
			MaxPedCooldown = max;
			GreenTiming    = greenTiming;
			RedTiming      = redTiming;
		}
	}

	#endregion

	#region Declare DTOs

	public bool IsInfinite       { get; }
	public int  BandWidth        { get; }
	public int  TicksCount       { get; }
	public int  TickSleepMs      { get; }
	public int  PedCrossingTicks { get; }
	public int  BandLength       { get; }

	public IRange View            { get; }
	public IRange CarFlowCooldown { get; }

	public List<ITrafficLightConfig>          TrafficLights        { get; } = new();
	public List<IUncontrolledCrosswalkConfig> CrossWalks           { get; } = new();
	public List<IControlledCrosswalkConfig>   ControlledCrossWalks { get; } = new();

	#endregion

	#region Define initialization logic

	/// <summary>
	/// Read config.json file and define all the properties
	/// </summary>
	/// <exception cref="Exception"></exception>
	public Config() {
		string raw = File.ReadAllText("../../../config.json", Encoding.UTF8);

		if (raw is null or "") {
			throw new Exception("Could not read config file");
		}

		JsonNode config = JsonNode.Parse(raw)!;

		if (config is null) {
			throw new Exception("Could not parse config file");
		}

		IsInfinite       = (bool)config["infinite"]!;
		BandWidth        = (int)config["bandWidth"]!;
		TicksCount       = (int)config["ticksCount"]!;
		TickSleepMs      = (int)config["tickSleepMs"]!;
		PedCrossingTicks = (int)config["pedCrossingTicks"]!;
		BandLength       = (int)config["bandLength"]!;

		View = new Range(
			(int)config["view"]!["from"]!,
			(int)config["view"]!["to"]!
		);

		CarFlowCooldown = new Range(
			(int)config["carFlow"]!["minCooldown"]!,
			(int)config["carFlow"]!["maxCooldown"]!
		);

		JsonArray lightsArray = config["trafficLights"]!.AsArray();
		foreach (JsonNode? lightNode in lightsArray) {
			TrafficLightConfig light = new(
				(int)lightNode!["at"]!,
				(int)lightNode["greenDuration"]!,
				(int)lightNode["redDuration"]!
			);

			TrafficLights.Add(light);
		}

		JsonArray crossWalksArray = config["uncontrolledCrosswalks"]!.AsArray();
		foreach (JsonNode? crossWalkNode in crossWalksArray) {
			UncontrolledCrossWalkConfig walk = new(
				(int)crossWalkNode!["at"]!,
				(int)crossWalkNode["minPedCooldown"]!,
				(int)crossWalkNode["maxPedCooldown"]!
			);

			CrossWalks.Add(walk);
		}

		JsonArray controlledCrossWalksArray = config["controlledCrosswalks"]!.AsArray();
		foreach (JsonNode? controlledCrossWalkNode in controlledCrossWalksArray) {
			ControlledCrossWalkConfig walk = new(
				(int)controlledCrossWalkNode!["at"]!,
				(int)controlledCrossWalkNode["minPedCooldown"]!,
				(int)controlledCrossWalkNode["maxPedCooldown"]!,
				(int)controlledCrossWalkNode["greenDuration"]!,
				(int)controlledCrossWalkNode["redDuration"]!
			);

			ControlledCrossWalks.Add(walk);
		}
	}

	#endregion
}
