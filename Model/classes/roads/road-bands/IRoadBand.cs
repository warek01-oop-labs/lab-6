using OOPLab6.Model.classes.helpers.simulation_dto;
using OOPLab6.Model.classes.road_objects.crosswalks;
using OOPLab6.Model.classes.road_objects.traffic_lights;
using OOPLab6.Model.classes.vehicles.Cars;

namespace OOPLab6.Model.classes.roads.road_bands;

public interface IRoadBand {
	int TotalCarFlow { get; }
	int CarsOnBand   { get; }
	int Length       { get; }

	List<ICar?>          Cars          { get; }
	List<ITrafficLight?> TrafficLights { get; }
	List<ICrosswalk?>    Crosswalks    { get; }

	bool HasCarAt(int          index);
	bool HasTrafficLightAt(int index);
	bool HasCrossWalkAt(int    index);

	bool MoveCar(ICar   car);
	bool PushCar(ICar   car);
	bool RemoveCar(ICar car);

	void AddLight(ITrafficLight light, int index);

	void AddCrosswalk(ICrosswalk walk, int index);

	void Tick(ISimulationDto dto);
}
