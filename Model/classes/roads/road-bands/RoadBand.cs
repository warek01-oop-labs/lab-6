using OOPLab6.Model.classes.helpers.simulation_dto;
using OOPLab6.Model.classes.road_objects.crosswalks;
using OOPLab6.Model.classes.road_objects.traffic_lights;
using OOPLab6.Model.classes.vehicles.Cars;

namespace OOPLab6.Model.classes.roads.road_bands;

public class RoadBand : IRoadBand {
	public int TotalCarFlow { get; private set; }
	public int CarsOnBand   => Cars.Count(car => car != null);
	public int Length       { get; }

	public List<ICar?>          Cars          { get; }
	public List<ITrafficLight?> TrafficLights { get; }
	public List<ICrosswalk?>    Crosswalks    { get; }

	public RoadBand(int length) {
		Length        = length;
		Cars          = new(new Car?[length]);
		TrafficLights = new(new TrafficLight?[length]);
		Crosswalks    = new(new Crosswalk?[length]);
	}

	public bool HasCarAt(int index) {
		return Cars[index] != null;
	}

	public bool HasTrafficLightAt(int index) {
		return TrafficLights[index] != null;
	}

	public bool HasCrossWalkAt(int index) {
		return Crosswalks[index] != null;
	}

	public bool MoveCar(ICar car) {
		int index = Cars.FindIndex(c => c == car);
		if (index == -1) {
			return false;
		}

		Cars[index]     = null;
		Cars[index + 1] = car;
		car.MoveTo(car.X, index + 1);

		return true;
	}

	public bool PushCar(ICar car) {
		if (Cars[0] != null) {
			return false;
		}

		car.MoveTo(0, 0);
		Cars[0] = car;

		return true;
	}

	public bool RemoveCar(ICar car) {
		int index = Cars.FindIndex(c => car == c);

		if (index == -1) {
			return false;
		}

		Cars[index] = null;
		TotalCarFlow++;

		return true;
	}

	public void AddLight(ITrafficLight light, int index) {
		if (TrafficLights[index] != null) {
			throw new Exception($"A light already placed at {index}");
		}

		TrafficLights[index] = light;
	}

	public void AddCrosswalk(ICrosswalk walk, int index) {
		if (Crosswalks[index] != null) {
			throw new Exception($"A crosswalk already placed at {index}");
		}

		Crosswalks[index] = walk;
	}

	public void Tick(ISimulationDto dto) {
		for (int i = Cars.Count - 1; i >= 0; i--) {
			if (Cars[i] == null) continue;

			ICar car = Cars[i]!;

			if (i >= Cars.Count - 1) {
				car.LeftBandAt          =  dto.Tick;
				dto.TotalLeaveTime      += car.LeftBandAt - car.EnteredBandAt;
				dto.TotalLightsTime     += car.WaitedOnRedLight;
				dto.TotalCrosswalksTime += car.WaitedOnCrosswalk;
				dto.TotalWaitTime       += car.Waited;
				RemoveCar(car);
				continue;
			}

			if (HasCarAt(i + 1)) {
				car.Waited++;
				continue;
			}

			if (HasTrafficLightAt(i + 1)) {
				ITrafficLight light = TrafficLights[i + 1]!;

				if (!light.IsGreen) {
					car.Waited++;
					car.WaitedOnRedLight++;
					continue;
				}
			}

			if (
				HasCrossWalkAt(i + 1)
				&& Crosswalks[i + 1]!.IsBeingCrossed
			) {
				car.Waited++;
				car.WaitedOnCrosswalk++;
				continue;
			}

			MoveCar(car);
		}

		foreach (ITrafficLight? light in TrafficLights) {
			light?.Tick();
		}

		foreach (ICrosswalk? walk in Crosswalks) {
			walk?.Tick();
		}

		dto.Tick++;
	}
}
