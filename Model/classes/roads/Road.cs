﻿using OOPLab6.Model.classes.roads.road_bands;

namespace OOPLab5.Model.classes.roads;

public class Road {
	private List<RoadBand> _bands = new();
	
	public List<RoadBand> Bands => _bands;

	public Road(int bandsCount, int length) {
		for (int i = 0; i < bandsCount; i++)
			_bands.Add(new RoadBand(length));
	}
}
