﻿using OOPLab6.Model.classes.roads.road_bands;

namespace OOPLab6.Model.classes.helpers.simulation_dto;

public interface ISimulationDto {
	IRoadBand RoadBand { get; }

	int TotalCarsGenerated  { get; set; }
	int Tick                { get; set; }
	int CarFlowCooldown     { get; set; }
	int CarsNotEnteredCount { get; set; }
	int TotalLeaveTime      { get; set; }
	int TotalCarsEntered    { get; set; }
	int TotalLightsTime     { get; set; }
	int TotalCrosswalksTime { get; set; }
	int TotalWaitTime       { get; set; }
}
