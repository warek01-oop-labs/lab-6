﻿using OOPLab6.Model.classes.roads.road_bands;

namespace OOPLab6.Model.classes.helpers.simulation_dto;

public class SimulationDto : ISimulationDto {
	public IRoadBand RoadBand            { get; }
	
	public int       TotalCarsGenerated  { get; set; }
	public int       Tick                { get; set; }
	public int       CarFlowCooldown     { get; set; }
	public int       CarsNotEnteredCount { get; set; }
	public int       TotalLeaveTime      { get; set; }
	public int       TotalCarsEntered    { get; set; }
	public int       TotalLightsTime     { get; set; }
	public int       TotalCrosswalksTime { get; set; }
	public int       TotalWaitTime       { get; set; }

	public SimulationDto(IRoadBand band) {
		RoadBand = band;
	}
}
