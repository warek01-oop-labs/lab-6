﻿namespace OOPLab6.Model.classes.helpers.FinalResults; 

public interface IFinalResult {

	public string ToString();
}
