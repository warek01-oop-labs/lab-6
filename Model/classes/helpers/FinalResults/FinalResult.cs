﻿using OOPLab6.Model.classes.helpers.simulation_dto;

namespace OOPLab6.Model.classes.helpers.FinalResults;

public class FinalResult : IFinalResult {
	private readonly ISimulationDto _simulationDto;

	private readonly double _avgLeaveTime;
	private readonly double _avgWaitTime;
	private readonly double _avgLightsTime;
	private readonly double _avgCrosswalksTime;

	public FinalResult(ISimulationDto simulationDto) {
		_simulationDto = simulationDto;

		_avgLeaveTime      = (double)_simulationDto.TotalLeaveTime / _simulationDto.TotalCarsEntered;
		_avgWaitTime       = (double)_simulationDto.TotalWaitTime  / _simulationDto.TotalCarsEntered;
		_avgLightsTime     = (double)_simulationDto.TotalWaitTime  / _simulationDto.TotalLightsTime;
		_avgCrosswalksTime = (double)_simulationDto.TotalWaitTime  / _simulationDto.TotalCrosswalksTime;
	}

	public override string ToString() {
		string resultStr = "";
		resultStr += $"Simulation ended after {_simulationDto.Tick} ticks\n";
		resultStr += $"Total generated cars: {_simulationDto.TotalCarsEntered}\n";
		resultStr += $"Cars currently on band: {_simulationDto.RoadBand.CarsOnBand}\n";
		resultStr += $"Cars exited band: {_simulationDto.RoadBand.TotalCarFlow}\n";
		resultStr += $"Cars not entered on band due to jam: {_simulationDto.CarsNotEnteredCount}\n";
		resultStr += $"Average time on band: {_avgLeaveTime:0.0}\n";
		resultStr += $"Average wait time: {_avgWaitTime:0.0}\n";
		resultStr += $"Average time on red light: {_avgLightsTime:0.0}\n";
		resultStr += $"Average time on crosswalks: {_avgCrosswalksTime:0.0}\n";

		return resultStr;
	}
}
