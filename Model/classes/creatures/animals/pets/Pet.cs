using OOPLab5.creatures.human;

namespace OOPLab6.Model.classes.creatures.animals.pets;

public class Pet : Animal, IPet {
	public Human BelongsTo;

	public Pet(Human     belongsTo, string species, string sound, int legsCount, AnimalType type,
	           DateTime? birthDate) : base(
		species, sound, legsCount, type, birthDate) {
		BelongsTo = belongsTo;
	}
}
