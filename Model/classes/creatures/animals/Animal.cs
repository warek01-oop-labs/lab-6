namespace OOPLab6.Model.classes.creatures.animals;

public enum AnimalType {
	Flying,
	Swimming,
	Crawling,
	Walking
}

public abstract class Animal : Creature, IAnimal {
	private bool _isHumanFriendly = false;

	public string Species { get; }

	public TimeSpan Age => DateTime.Now - BirthDate;

	public bool WillBiteHumans {
		get => !_isHumanFriendly;
		init => _isHumanFriendly = !value;
	}

	public bool HasFur { get; init; } = false;

	public AnimalType Type { get; }

	public string[] ContinentsOfHabitation { get; init; } =
		{ "Europe", "Asia", "Middle East", "Africa", "North America", "South America" };

	public Animal(string species, string sound, int legsCount, AnimalType type, DateTime? birthDate)
		: base(sound, 1, .5, birthDate) {
		Species = species;
		Type    = type;
	}
}
