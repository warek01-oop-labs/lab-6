using OOPLab6.Model.classes.vehicles.Cars;

namespace OOPLab5.Model.classes.creatures.humans.driver; 

public interface IDriver {
	public void SitInCar(Car car);
	public void Say();
}
