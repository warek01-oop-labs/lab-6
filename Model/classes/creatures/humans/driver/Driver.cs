using OOPLab5.Model.classes.creatures.humans.driver;
using OOPLab5.creatures.human;
using OOPLab6.Model.classes.vehicles.Cars;

namespace OOPLab6.Model.classes.creatures.humans.driver;

public class Driver : Human, IDriver {
	private List<Car> _carsList;

	public List<Car> OwnedCars => _carsList;

	public Car CurrentCar { get; private set; }

	public Driver(string fname, string lname, DateTime bdate, List<Car> cars, Car currentCar)
		: base(fname, lname, bdate) {
		_carsList       = cars;
		CurrentCar = currentCar;
	}

	public void SitInCar(Car car) {
		if (!_carsList.Contains(car)) {
			Console.WriteLine($"{FullName} does not own {car.CarInfo}");
			return;
		}
		
		CurrentCar = car;
	}
	
	public override void Say() {
		Console.WriteLine($"{FullName} says something about cars");
	}
}
