namespace OOPLab6.Model.classes.creatures.humans;

public interface IHuman {
	void Walk();
	void Stop();
	void Present();
	void Eat();
	void Run();
	void Grow();
	void ChangeJob(string job);
	void Say();
}
