using OOPLab6.Model.classes.root;

namespace OOPLab6.Model.classes.creatures;

public abstract class Creature : Movable, ICreature {
	private double _height;
	private double _weight;
	private string _sound;
	
	public DateTime BirthDate { get; }
	
	public double Height {
		get => _height;
		protected set {
			if (value < 0)
				throw new Exception("Height can't be negative");
			_height = value;
		}
	}
	
	public double Weight {
		get => _weight;
		protected set {
			if (value < 0)
				throw new Exception("Weight can't be negative");
			_weight = value;
		}
	}
	
	public Creature(string sound, double defaultWeight, double defaultHeight, DateTime? bdate) {
		BirthDate = bdate.GetValueOrDefault(DateTime.Now);
		Weight    = defaultHeight;
		Height    = defaultHeight;
		_sound    = sound;
	}
	
	public virtual void Say() {
		Console.WriteLine(_sound);
	}
}
