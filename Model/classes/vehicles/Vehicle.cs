using OOPLab6.Model.classes.root;

namespace OOPLab6.Model.classes.vehicles;

public abstract class Vehicle : Movable, IVehicle {
	private readonly string _type;
	private readonly int    _weight;

	public int WaitedOnRedLight  { get; set; }
	public int WaitedOnCrosswalk { get; set; }
	public int Waited            { get; set; }
	public int EnteredBandAt     { get; set; }
	public int LeftBandAt        { get; set; }

	public int DoorsCount  { get; }
	public int WheelsCount { get; }

	public string VehicleInfo => $"{_type} has {DoorsCount} doors and {WheelsCount} wheels";

	public int Weight {
		get => _weight;
		init {
			if (value < 0)
				throw new Exception("Weight can't be negative");

			_weight = value;
		}
	}

	protected Vehicle(int doorsCount, int wheelsCount, string type, int defaultWeight) {
		if (doorsCount < 0) {
			throw new Exception("Doors count can't be negative");
		}

		if (wheelsCount < 0) {
			throw new Exception("Wheels count can't be negative");
		}

		if (defaultWeight < 0) {
			throw new Exception("Weight can't be negative");
		}

		DoorsCount  = doorsCount;
		WheelsCount = wheelsCount;
		_type       = type;
		_weight     = defaultWeight;
	}

	public void MoveTo(int x, int y) {
		ChangePosition(x, y);
	}
}
