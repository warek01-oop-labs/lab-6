using OOPLab6.Model.classes.root;

namespace OOPLab6.Model.classes.vehicles;

public interface IVehicle : IMovable {
	int    DoorsCount  { get; }
	int    WheelsCount { get; }
	string VehicleInfo { get; }
	int    Weight      { get; init; }

	int WaitedOnRedLight  { get; set; }
	int WaitedOnCrosswalk { get; set; }
	int Waited            { get; set; }
	int EnteredBandAt     { get; set; }
	int LeftBandAt        { get; set; }

	void MoveTo(int x, int y);
}
