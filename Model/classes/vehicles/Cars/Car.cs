namespace OOPLab6.Model.classes.vehicles.Cars;

public abstract class Car : Vehicle, ICar {
	public int    MaxSpeed  { get; init; }
	public int    Height    { get; init; }
	public int    Width     { get; init; }
	public int    Clearance { get; init; }
	public int    Length    { get; init; }
	public string BrandName { get; }
	public string CarType   { get; }

	public string CarInfo =>
		$"{BrandName} is {CarType}, has {DoorsCount} doors, {Weight} kg, max speed of {MaxSpeed} km/h";

	protected Car(int doorsCount, string type, string brandName)
		: base(doorsCount, 4, "Car", 1200) {
		CarType   = type;
		BrandName = brandName;
	}

	public virtual void PresentCar() {
		Console.WriteLine(CarInfo);
	}

	public override string ToString() {
		return $"{CarType} {BrandName}";
	}
}
