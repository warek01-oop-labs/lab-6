namespace OOPLab6.Model.classes.vehicles.Cars;

public class Crossover : Car {
	public Crossover(string brandName) : base(4, "Crossover", brandName) { }
}
