namespace OOPLab6.Model.classes.vehicles.Cars;

public interface ICar: IVehicle {
	int    MaxSpeed  { get; init; }
	int    Height    { get; init; }
	int    Width     { get; init; }
	int    Clearance { get; init; }
	int    Length    { get; init; }
	string BrandName { get; }
	string CarInfo   { get; }
	string CarType   { get; }

	void PresentCar();
}
