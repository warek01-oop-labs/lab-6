namespace OOPLab6.Model.classes.vehicles.Cars;

public class Sedan : Car {
	public Sedan(string brandName) : base(4, "Sedan", brandName) { }
}
