namespace OOPLab6.Model.classes.road_objects.traffic_lights;

public class TrafficLight : RoadObject, ITrafficLight {
	private readonly int  _greenTiming;
	private readonly int  _redTiming;
	private          bool _isGreen = true;
	private          int  _tick    = 0;
	public           bool IsGreen => _isGreen;

	public TrafficLight(int greenTiming, int redTiming) {
		_greenTiming = greenTiming;
		_redTiming   = redTiming;
	}

	public void Tick() {
		if ((IsGreen     && _tick >= _greenTiming)
		    || (!IsGreen && _tick >= _redTiming)) {
			_isGreen = !_isGreen;
			_tick    = 0;
		}
		else {
			_tick++;
		}
	}
}
