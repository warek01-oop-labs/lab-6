namespace OOPLab6.Model.classes.road_objects.traffic_lights;

public interface ITrafficLight : IRoadObject {
	bool IsGreen { get; }

	void Tick();
}
