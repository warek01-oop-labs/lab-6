using OOPLab5.creatures.human;
using OOPLab6.Model.classes.helpers;

namespace OOPLab6.Model.classes.road_objects.crosswalks;

public abstract class Crosswalk : RoadObject, ICrosswalk {
	private readonly int _minPedCooldown;
	private readonly int _maxPedCooldown;
	private          int _cooldown;

	public List<Human> Peds { get; } = new();

	public int PedsCount => Peds.Count;

	public bool IsBeingCrossed { get; private set; }


	protected Crosswalk(int min, int max) {
		_minPedCooldown = min;
		_maxPedCooldown = max;
	}

	public virtual void Tick() {
		if (_cooldown == 0) {
			Random rand = new();
			Peds.Add(Generate.Ped());
			_cooldown = rand.Next(_minPedCooldown, _maxPedCooldown);
		}
		else {
			_cooldown--;
		}
	}

	public virtual void StartCrossing() {
		IsBeingCrossed = true;
	}

	public virtual void StopCrossing() {
		IsBeingCrossed = false;
	}
}
