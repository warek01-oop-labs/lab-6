﻿using OOPLab6.Model.classes.road_objects.traffic_lights;

namespace OOPLab6.Model.classes.road_objects.crosswalks.controlled_crosswalks;

public class ControlledCrossWalk : Crosswalk, IControlledCrossWalk {
	public TrafficLight TrafficLight { get; }

	public ControlledCrossWalk(TrafficLight light, int min, int max)
		: base(min, max) {
		TrafficLight = light;
	}

	public override void Tick() {
		base.Tick();
		TrafficLight.Tick();

		if (!TrafficLight.IsGreen)
			Peds.Clear();
	}
}
