﻿using OOPLab6.Model.classes.road_objects.traffic_lights;

namespace OOPLab6.Model.classes.road_objects.crosswalks.controlled_crosswalks;

public interface IControlledCrossWalk : ICrosswalk {
	TrafficLight TrafficLight { get; }

	new void Tick();
}
