﻿using OOPLab6.Model;

namespace OOPLab6.Model.classes.road_objects.crosswalks.uncontrolled_crosswalk;

public class UncontrolledCrosswalk : Crosswalk, IUncontrolledCrosswalk {
	private readonly IConfig _config;

	private int _crossTicksRemaining;

	public UncontrolledCrosswalk(int min, int max, IConfig config) : base(min, max) {
		_config = config;
	}

	public override void Tick() {
		int prevCount = PedsCount;
		base.Tick();

		if (PedsCount > prevCount) {
			_crossTicksRemaining += _config.PedCrossingTicks;
			StartCrossing();
		}

		if (_crossTicksRemaining > 0) {
			_crossTicksRemaining--;
		}
		else {
			StopCrossing();
			Peds.Clear();
		}
	}
}
