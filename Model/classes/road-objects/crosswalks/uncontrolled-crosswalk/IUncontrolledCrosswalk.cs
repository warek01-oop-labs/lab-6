﻿namespace OOPLab6.Model.classes.road_objects.crosswalks.uncontrolled_crosswalk; 

public interface IUncontrolledCrosswalk {
	void Tick();
}
