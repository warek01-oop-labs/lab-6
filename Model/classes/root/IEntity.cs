﻿namespace OOPLab6.Model.classes.root; 

public interface IEntity {
	int X { get; }
	int Y { get; }
}
