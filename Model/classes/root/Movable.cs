namespace OOPLab6.Model.classes.root;

public abstract class Movable : Entity, IMovable {
	protected void ChangePosition(int x, int y) {
		X = x;
		Y = y;
	}
}
