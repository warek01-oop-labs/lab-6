namespace OOPLab6.Model.classes.root;

public abstract class Entity : IEntity {
	public int X { get; protected set; }
	public int Y { get; protected set; }
}
