﻿namespace OOPLab6.Model.config_dtos;

public interface IRange {
	int From { get; }
	int To   { get; }
}
