﻿namespace OOPLab6.Model.config_dtos; 

public interface ITrafficLightConfig {
	int At            { get; }
	int GreenDuration { get; }
	int RedDuration   { get; }
}
