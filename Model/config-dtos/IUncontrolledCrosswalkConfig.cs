﻿namespace OOPLab6.Model.config_dtos; 

public interface IUncontrolledCrosswalkConfig {
	int At             { get; }
	int MinPedCooldown { get; }
	int MaxPedCooldown { get; }
}
