﻿namespace OOPLab6.Model.config_dtos;

public interface IControlledCrosswalkConfig {
	int At             { get; }
	int MinPedCooldown { get; }
	int MaxPedCooldown { get; }
	int GreenTiming    { get; }
	int RedTiming      { get; }
}
