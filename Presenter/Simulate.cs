using System.Text;
using OOPLab6.Model.classes.helpers;
using OOPLab6.Model.classes.road_objects.crosswalks.controlled_crosswalks;
using OOPLab6.Model.classes.road_objects.crosswalks.uncontrolled_crosswalk;
using OOPLab6.Model.classes.road_objects.traffic_lights;
using OOPLab6.Model.classes.roads.road_bands;
using OOPLab6.Model.classes.vehicles.Cars;
using OOPLab6.Model;
using OOPLab6.Model.classes.helpers.FinalResults;
using OOPLab6.Model.classes.helpers.simulation_dto;
using OOPLab6.Model.config_dtos;
using OOPLab6.View;

namespace OOPLab6.Presenter;

/// <summary>
/// Presenter component of the simulation
/// </summary>
public class Simulate : ISimulate {
	#region Declare dependencies

	private readonly IConfig _config;
	private readonly IRender _render;

	#endregion

	#region Declare DTOs

	private readonly ISimulationDto _dto;

	#endregion

	public Simulate(IConfig config, IRender render) {
		_config = config;
		_render = render;

		_dto = new SimulationDto(new RoadBand(_config.BandLength));
	}

	public void Run() {
		Random rand = new();

		#region Populate Road Band

		foreach (ITrafficLightConfig light in _config.TrafficLights)
			_dto.RoadBand.AddLight(
				new TrafficLight(light.GreenDuration, light.RedDuration),
				light.At
			);

		foreach (IUncontrolledCrosswalkConfig walk in _config.CrossWalks)
			_dto.RoadBand.AddCrosswalk(
				new UncontrolledCrosswalk(walk.MinPedCooldown, walk.MaxPedCooldown, _config),
				walk.At
			);

		foreach (IControlledCrosswalkConfig walk in _config.ControlledCrossWalks) {
			TrafficLight light = new(walk.GreenTiming, walk.RedTiming);

			_dto.RoadBand.AddCrosswalk(
				new ControlledCrossWalk(light, walk.MinPedCooldown, walk.MaxPedCooldown),
				walk.At
			);

			_dto.RoadBand.AddLight(light, walk.At);
		}

		#endregion

		#region Simulate each tick

		while (true) {
			if (Console.KeyAvailable) {
				ConsoleKey key = Console.ReadKey(true).Key;
				if (key == ConsoleKey.Q) break;
			}

			if (_dto.CarFlowCooldown == 0) {
				Car car = Generate.Car();
				car.MoveTo(_config.BandWidth / 2, car.Y);
				car.EnteredBandAt = _dto.Tick;

				if (!_dto.RoadBand.PushCar(car)) {
					_dto.CarsNotEnteredCount++;
				}
				else {
					_dto.TotalCarsEntered++;
				}

				_dto.TotalCarsGenerated++;

				_dto.CarFlowCooldown = rand.Next(
					_config.CarFlowCooldown.From,
					_config.CarFlowCooldown.To
				);
			}
			else {
				_dto.CarFlowCooldown--;
			}

			_dto.RoadBand.Tick(_dto);
			_render.DrawBand(_dto.RoadBand);

			if (!_config.IsInfinite && _dto.Tick == _config.TicksCount) {
				break;
			}
			
			Thread.Sleep(_config.TickSleepMs);
		}

		#endregion

		#region Compute results & output them

		IFinalResult finalResult = new FinalResult(_dto);

		File.WriteAllText("results.txt", finalResult.ToString(), Encoding.UTF8);
		_render.DrawEndScreen(finalResult);

		#endregion
	}
}
