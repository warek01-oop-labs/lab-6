﻿namespace OOPLab6.Presenter;

public interface ISimulate {
	void Run();
}
